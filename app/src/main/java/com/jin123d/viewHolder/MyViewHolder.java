package com.jin123d.viewHolder;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.MotionEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jin123d.hekrdemo.R;
import com.jin123d.itemTouchHelper.ItemTouchHelperViewHolderListener;
import com.jin123d.itemTouchHelper.ItemTouchMoveListener;
import com.jin123d.listener.MyItemClickListener;
import com.jin123d.listener.MyItemLongClickListener;
import com.jin123d.listener.TouchListener;

/**
 * Created by jin123d on 2016/2/29.
 */
public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener, ItemTouchHelperViewHolderListener{

    public TextView tv;
    private MyItemClickListener mListener;
    private MyItemLongClickListener mLongClickListener;

    public MyViewHolder(View view, MyItemClickListener listener, MyItemLongClickListener longClickListener) {
        super(view);
        tv = (TextView) view.findViewById(R.id.tv_name);
        this.mListener = listener;
        this.mLongClickListener = longClickListener;
        view.setOnClickListener(this);
        view.setOnLongClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (mListener != null) {
            mListener.onItemClick(view, getPosition());
        }
    }

    @Override
    public boolean onLongClick(View view) {
        if (mLongClickListener != null) {
            mLongClickListener.onItemLongClick(view, getPosition());
        }
        return true;
    }

    @Override
    public void onItemSelected() {
        itemView.setBackgroundColor(Color.LTGRAY);
    }

    @Override
    public void onItemClear() {
        itemView.setBackgroundColor(0);
    }


}
