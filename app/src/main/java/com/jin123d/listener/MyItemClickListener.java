package com.jin123d.listener;

import android.view.View;

/**
 * Created by jin123d on 2016/2/29.
 */
public interface MyItemClickListener {
    void onItemClick(View view, int position);
}
