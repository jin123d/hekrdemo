package com.jin123d.listener;

import android.view.View;

/**
 * Created by jin123d on 2016/2/29.
 */
public interface MyItemLongClickListener {
    void onItemLongClick(View view, int position);
}
