package com.jin123d.hekrdemo;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jin123d.adapter.DevicesListAdapter;
import com.jin123d.bean.MainBean;
import com.jin123d.fragment.SlidingMenuFragment;
import com.jin123d.itemTouchHelper.ItemTouchHelperCallback;
import com.jin123d.itemTouchHelper.OnStartDragListener;
import com.jin123d.ui.CircularAuthProgressBar;
import com.jin123d.ui.CircularOnlineProgressBar;

import java.util.ArrayList;
import java.util.List;

public class ScrollingActivity extends AppCompatActivity implements OnStartDragListener {
    private LinearLayout linearLayout;
    private LinearLayout titleDevicesNumber;
    private RecyclerView mRecyclerView;
    private Toolbar toolbar;
    public static SlidingMenu slidingMenu;
    private AppBarLayout appBarLayout;
    private CircularOnlineProgressBar circularOnlineProgressBar;
    private CircularAuthProgressBar circularAuthProgressBar;
    private SwipeRefreshLayout swipeRefreshLayout;
    private List<MainBean> data;
    private DevicesListAdapter adapter;
    private ItemTouchHelper mItemTouchHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        initView();
        initSling();
        move();


        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                int maxScroll = appBarLayout.getTotalScrollRange();
                if (verticalOffset + maxScroll == 0) {
                    titleDevicesNumber.setVisibility(View.VISIBLE);
                } else {
                    titleDevicesNumber.setVisibility(View.GONE);
                }

            }
        });


    }

    //初始化
    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        linearLayout = (LinearLayout) findViewById(R.id.layout_device);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mRecyclerView = (RecyclerView) findViewById(R.id.conversation);
        titleDevicesNumber = (LinearLayout) findViewById(R.id.title_devices_number);
        circularOnlineProgressBar = (CircularOnlineProgressBar) findViewById(R.id.probar_1);
        circularAuthProgressBar = (CircularAuthProgressBar) findViewById(R.id.probar_2);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        //设置两个圈圈的属性
        circularOnlineProgressBar.setMax(9);
        circularOnlineProgressBar.setProgress(6);
        circularAuthProgressBar.setMax(circularOnlineProgressBar.getMax());
        circularAuthProgressBar.setProgress(3, 3);
        //默认设置布局为3列
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3);

        adapter = new DevicesListAdapter(ScrollingActivity.this, getData());
        if (null == getData() || 0 == getData().size()) {
            gridLayoutManager.setSpanCount(2);
        }

        mRecyclerView.setAdapter(adapter);
        //mRecyclerView.addItemDecoration(new HorizontalDividerItemDecoration.Builder(this)
        //      .color(Color.WHITE).build());
        // mRecyclerView.addItemDecoration(new VerticalDividerItemDecoration.Builder(this).color(Color.WHITE).build());
        mRecyclerView.setLayoutManager(gridLayoutManager);
        toolbar.setNavigationIcon(R.mipmap.menu);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                slidingMenu.toggle();
            }
        });
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(false);
            }
        });

    }

    //侧边栏 以fragment的形式出现
    private void initSling() {
        slidingMenu = new SlidingMenu(ScrollingActivity.this);
        slidingMenu.setMode(SlidingMenu.LEFT);
        slidingMenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        slidingMenu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        slidingMenu.setMenu(R.layout.layout_left_menu);
        slidingMenu.setBehindOffsetRes(R.dimen.sliding_menu_off_set);
        getSupportFragmentManager().beginTransaction().replace(R.id.slidingmenumain, new SlidingMenuFragment()).commit();

    }

    //此处模拟设备
    private List<MainBean> getData() {
        data = new ArrayList<>();
        for (int i = 0; i <= 14; i++) {
            MainBean mainBean = new MainBean("设备" + i, MainBean.TYPE_DEVICES);
            // MainBean mainBean2 = new MainBean("目录" + i, false, MainBean.TYPE_FOLDER);
            data.add(mainBean);
            // data.add(mainBean2);
        }
        return data;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }


    private void move() {
        ItemTouchHelper.Callback callback = new ItemTouchHelperCallback(adapter);
        //绑定到控件中
        mItemTouchHelper = new ItemTouchHelper(callback);
        mItemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    @Override
    public void onStartDrag(RecyclerView.ViewHolder viewHolder) {
        mItemTouchHelper.startDrag(viewHolder);
    }
}
