package com.jin123d.hekrdemo;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

public class LoadingActivity extends AppCompatActivity {
    private boolean enableRefresh;
    private ImageView image;
    private int position = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        image = (ImageView) findViewById(R.id.image);

        final ObjectAnimator oa = (ObjectAnimator) AnimatorInflater.loadAnimator(LoadingActivity.this,
                R.animator.fanzhuan);
        final ObjectAnimator oa_2 = (ObjectAnimator) AnimatorInflater.loadAnimator(LoadingActivity.this,
                R.animator.fanzhuan_2);
        oa.setTarget(image);
        oa_2.setTarget(image);
        oa.start();

        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setAni(position);
                oa_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        oa_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                setAni(position);
                oa.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    private void setAni(int p) {
        if (p >= 3) {
            position = 0;
        } else {
            position++;
        }
        Log.d("position", position + "");
        switch (position) {
            case 0:
                image.setImageResource(R.mipmap.btn_weibo);
                break;
            case 1:
                image.setImageResource(R.mipmap.btn_facebook);
                break;
            case 2:
                image.setImageResource(R.mipmap.btn_qq);
                break;
            case 3:
                image.setImageResource(R.mipmap.btn_twitter);
                break;
        }
    }

}
