package com.jin123d.bean;

/**
 * Created by jin123d on 2016/3/2.
 **/
public class MainBean {
    private String name; //名称
    private int type; //类型 1为设备 2为目录

    public static final int TYPE_DEVICES = 1;
    public static final int TYPE_FOLDER = 2;

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MainBean(String name, int type) {
        this.name = name;
        this.type = type;
    }
}
