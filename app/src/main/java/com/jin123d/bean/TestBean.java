package com.jin123d.bean;

/**
 * Created by jin123d on 2016/3/4.
 **/
public class TestBean {
    /**
     * fid : 0
     * uid : 14a6841964cf20d36ef98625b06f1aea
     * model : {"id":31,"logo_url":{"normal":""},"android_h5_url":"http://app.hekr.me/android/31/index.html","name":"M001","ios_h5_url":"http://app.hekr.me/android/31/index.html"}
     * detail : {"mname":"FXM_CZ","phone":"","model":"","binver":"","host":"","provider":"","pid":"12","cid":"50","category":"","bintype":"","tid":"ESP_2M_1AFE34F3560C","mid":"31","mk":"","softapkey":""}
     * ios_h5_url : http://app.hekr.me/android/31/index.html
     * state : {"timertodo":1,"power":1,"timer":0}
     * online : 1
     * id : 50
     * schedulerRuleTag : 0
     * category : {"ename":"Socket","id":50,"updated_at":"2016-02-18 02:47:01","logo_url":{"normal":"http://7xl8jv.com2.z0.glb.qiniucdn.com/1455778020429iconfont-hekriconshebeichazuo.png"},"name":"插座"}
     * time : 1457073023173
     * updated_at : 2016-02-18 02:47:01
     * android_h5_url : http://app.hekr.me/android/31/index.html
     * name : 正滚滚滚g
     * ename : Socket
     * tid : ESP_2M_1AFE34F3560C
     * logo_url : {"normal":"http://7xl8jv.com2.z0.glb.qiniucdn.com/1455778020429iconfont-hekriconshebeichazuo.png"}
     * fname : all
     * binVersion : 2.99.63.3(A)
     */

    private int fid;
    private String uid;
    /**
     * mname : FXM_CZ
     * phone :
     * model :
     * binver :
     * host :
     * provider :
     * pid : 12
     * cid : 50
     * category :
     * bintype :
     * tid : ESP_2M_1AFE34F3560C
     * mid : 31
     * mk :
     * softapkey :
     */

    private DetailEntity detail;
    /**
     * timertodo : 1
     * power : 1
     * timer : 0
     */

    private StateEntity state;
    private int online;
    private int schedulerRuleTag;
    private String name;
    private String tid;
    /**
     * normal : http://7xl8jv.com2.z0.glb.qiniucdn.com/1455778020429iconfont-hekriconshebeichazuo.png
     */

    private LogoUrlEntity logo_url;
    private String binVersion;

    public void setFid(int fid) {
        this.fid = fid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setDetail(DetailEntity detail) {
        this.detail = detail;
    }

    public void setState(StateEntity state) {
        this.state = state;
    }

    public void setOnline(int online) {
        this.online = online;
    }

    public void setSchedulerRuleTag(int schedulerRuleTag) {
        this.schedulerRuleTag = schedulerRuleTag;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTid(String tid) {
        this.tid = tid;
    }

    public void setLogo_url(LogoUrlEntity logo_url) {
        this.logo_url = logo_url;
    }

    public void setBinVersion(String binVersion) {
        this.binVersion = binVersion;
    }

    public int getFid() {
        return fid;
    }

    public String getUid() {
        return uid;
    }

    public DetailEntity getDetail() {
        return detail;
    }

    public StateEntity getState() {
        return state;
    }

    public int getOnline() {
        return online;
    }

    public int getSchedulerRuleTag() {
        return schedulerRuleTag;
    }

    public String getName() {
        return name;
    }

    public String getTid() {
        return tid;
    }

    public LogoUrlEntity getLogo_url() {
        return logo_url;
    }

    public String getBinVersion() {
        return binVersion;
    }

    public static class DetailEntity {
        private String binver;

        public void setBinver(String binver) {
            this.binver = binver;
        }

        public String getBinver() {
            return binver;
        }
    }

    public static class StateEntity {
        private int power;

        public void setPower(int power) {
            this.power = power;
        }

        public int getPower() {
            return power;
        }
    }

    public static class LogoUrlEntity {
        private String normal;

        public void setNormal(String normal) {
            this.normal = normal;
        }

        public String getNormal() {
            return normal;
        }
    }

    public TestBean(int fid, String uid, DetailEntity detail, StateEntity state, int online, int schedulerRuleTag, String name, String tid, LogoUrlEntity logo_url, String binVersion) {
        this.fid = fid;
        this.uid = uid;
        this.detail = detail;
        this.state = state;
        this.online = online;
        this.schedulerRuleTag = schedulerRuleTag;
        this.name = name;
        this.tid = tid;
        this.logo_url = logo_url;
        this.binVersion = binVersion;
    }


}
