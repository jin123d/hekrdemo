package com.jin123d.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by jin123d on 2016/2/25.
 */
public class CircularAuthProgressBar extends View {
    //全部设备数量
    private int mAllDevicesNumber = 100;
    //已授权数量
    private int mListAuthDeviceNumber = 0;
    //被授权数量
    private int mAuthDevicesNumber = 0;

    private Paint mPaint = new Paint();
    private RectF mRectF = new RectF();
    //背景色
    private int mBackgroundColor = Color.WHITE;
    //已授权颜色
    private int mListPrimaryColor = Color.parseColor("#16b2a5");
    //被授权颜色
    private int mAuthColor = Color.parseColor("#acd14c");
    //画笔宽度
    private float mStrokeWidth = 15f;

    /**
     * 进度条改变监听
     */
    public interface OnProgressChangeListener {
        /**
         * 进度改变事件，当进度条进度改变，就会调用该方法
         *
         * @param duration              总进度
         * @param listAuthDevicesNumber 已授权
         * @param authDevicesNumber     被授权
         * @param rateListAuth          已授权进度 即：rate = (float)(listAuthDevicesNumber) / duration
         * @param rateAuth              被授权进度
         */
        public void onChange(int duration, int listAuthDevicesNumber, int authDevicesNumber, float rateListAuth, float rateAuth);
    }

    private OnProgressChangeListener mOnChangeListener;

    /**
     * 设置进度条改变监听
     *
     * @param l
     */
    public void setOnProgressChangeListener(OnProgressChangeListener l) {
        mOnChangeListener = l;
    }

    public CircularAuthProgressBar(Context context) {
        super(context);
    }

    public CircularAuthProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        /*mPaint = new Paint();
        TypedArray typedArray = context.obtainStyledAttributes(attrs,
                R.styleable.CircularProgressBarView);
        mStrokeWidth = typedArray.getFloat(R.styleable.CircularProgressBarView_circleWidth, 10f);
        mListPrimaryColor = typedArray.getColor(R.styleable.CircularProgressBarView_primaryColor, 0XFFFFFF);
        mBackgroundColor = typedArray.getColor(R.styleable.CircularProgressBarView_backgroundColor, Color.LTGRAY);
        typedArray.recycle();*/
    }

    /**
     * 设置进度条的最大值, 该值要 大于 0
     *
     * @param max
     */
    public void setMax(int max) {
        if (max < 0) {
            max = 0;
        }
        mAllDevicesNumber = max;
    }

    /**
     * 得到进度条的最大值
     *
     * @return
     */
    public int getMax() {
        return mAllDevicesNumber;
    }

    /**
     * 设置进度条的当前的已授权和被授权数量
     *
     * @param mListAuthDeviceNumber 已授权数量
     * @param mAuthDevicesNumber    被授权数量
     */
    public void setProgress(int mListAuthDeviceNumber, int mAuthDevicesNumber) {
        if ((mListAuthDeviceNumber + mAuthDevicesNumber) > mAllDevicesNumber) {
            mAllDevicesNumber = mListAuthDeviceNumber + mAuthDevicesNumber;
        }
        //被授权数量
        this.mAuthDevicesNumber = mAuthDevicesNumber;
        //已授权数量
        this.mListAuthDeviceNumber = mListAuthDeviceNumber;
        if (mOnChangeListener != null) {
            mOnChangeListener.onChange(mAllDevicesNumber, mListAuthDeviceNumber, mAuthDevicesNumber, getRateOfListAuthDevicesNumber(), getRateOfAuthDevicesNumber());
        }
        invalidate();
    }

    /**
     * 得到进度条当前的值
     *
     * @return
     */
    public int getProgress() {
        return mListAuthDeviceNumber + mAuthDevicesNumber;
    }

    /**
     * 设置进度条背景的颜色
     */
    public void setBackgroundColor(int color) {
        mBackgroundColor = color;
    }

    /**
     * 设置进度条进度的颜色
     */
    public void setmListPrimaryColor(int color) {
        mListPrimaryColor = color;
    }

    /**
     * 设置环形的宽度
     *
     * @param width
     */
    public void setCircleWidth(float width) {
        mStrokeWidth = width;

    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int halfWidth = getWidth() / 2;
        int halfHeight = getHeight() / 2;
        int radius = halfWidth < halfHeight ? halfWidth : halfHeight;
        float halfStrokeWidth = mStrokeWidth / 2;

        // 设置画笔
        mPaint.setColor(mBackgroundColor);
        mPaint.setDither(true);
        mPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
        mPaint.setAntiAlias(true);
        mPaint.setStrokeWidth(mStrokeWidth);
        mPaint.setStyle(Paint.Style.STROKE);    //设置图形为空心

        // 画背景
        canvas.drawCircle(halfWidth, halfHeight, radius - halfStrokeWidth, mPaint);

        // 画当前进度的圆环
        // 改变画笔颜色--->授权设备颜色
        mPaint.setColor(mListPrimaryColor);
        mRectF.top = halfHeight - radius + halfStrokeWidth;
        mRectF.bottom = halfHeight + radius - halfStrokeWidth;
        mRectF.left = halfWidth - radius + halfStrokeWidth;
        mRectF.right = halfWidth + radius - halfStrokeWidth;
        /**
         * 绘制出来已授权的设备比率
         * 第一个角度为开始角度，第二个角度为绘制角度
         * 比如从90度开始绘制，再绘制30度。水平为0度，顺时针开始。
         * */
        if (mListAuthDeviceNumber != 0) {
            canvas.drawArc(mRectF, 270 - getRateOfListAuthDevicesNumber() * 360, getRateOfListAuthDevicesNumber() * 360, false, mPaint);
        }
        // 改变画笔颜色--->被授权设备颜色
        mPaint.setColor(mAuthColor);
        //绘制出被授权的设备比率
        if (mAuthDevicesNumber != 0) {
            canvas.drawArc(mRectF, 270 - getRateOfListAuthDevicesNumber() * 360 - getRateOfAuthDevicesNumber() * 360, getRateOfAuthDevicesNumber() * 360, false, mPaint);
        }
        canvas.save();
    }

    /**
     * 得到当前的已授权进度的比率
     * <p> 用进度条当前的值 与 进度条的最大值求商 </p>
     *
     * @return
     */
    private float getRateOfListAuthDevicesNumber() {
        return (float) mListAuthDeviceNumber / mAllDevicesNumber;
    }

    /**
     * 得到当前被授权设备进度的比率
     */
    private float getRateOfAuthDevicesNumber() {
        return (float) mAuthDevicesNumber / mAllDevicesNumber;
    }

}
