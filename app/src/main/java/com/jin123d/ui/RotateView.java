package com.jin123d.ui;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

import com.jin123d.hekrdemo.R;

import java.util.ArrayList;

/**
 * Created by lxh on 2015/10/26.
 */
public class RotateView extends View {

    private static final int DURATION = 1000;

    private ArrayList<Bitmap> unusedData;
    private Context mContext;


    private ObjectAnimator animator;

    public RotateView(Context context) {
        super(context);
        init(context);
    }

    public RotateView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public RotateView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        unusedData = new ArrayList<Bitmap>();
        mContext = context;
    }

    public void setData(ArrayList<Bitmap> datas) {
        if (datas == null || datas.size() < 4) {
            throw new RuntimeException("data size must be more than four");
        }
        int size = datas.size();
        for (int i = 0; i < size; i++) {
            unusedData.add(datas.get(i));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

    }

    /**
     * 此方法释放对数据的引用，不过调用之后需要重新setData之后才可以start
     */
    public void destory() {
        animator.cancel();
        animator = null;
        unusedData.clear();
        unusedData = null;
    }


    public void start() {
        final int ani = 0;
        if (unusedData.size() == 0) {
            throw new RuntimeException("before start must set data");
        }
        final ObjectAnimator oa = (ObjectAnimator) AnimatorInflater.loadAnimator(mContext,
                R.animator.fanzhuan);
        final ObjectAnimator oa_2 = (ObjectAnimator) AnimatorInflater.loadAnimator(mContext,
                R.animator.fanzhuan_2);
        oa.setTarget(RotateView.this);
        oa.start();

        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                oa_2.setTarget(setAni(ani));
                oa_2.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
        oa_2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                oa.setTarget(setAni(ani));
                oa.start();
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }


    private Object setAni(int position) {
        if (position >= unusedData.size()) {
            position = 0;
        } else {
            position++;
        }
        return unusedData.get(position);
    }


}