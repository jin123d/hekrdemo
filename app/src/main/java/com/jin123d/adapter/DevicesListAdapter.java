package com.jin123d.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.jin123d.bean.MainBean;
import com.jin123d.hekrdemo.R;
import com.jin123d.itemTouchHelper.ItemTouchHelperAdapterListener;
import com.jin123d.itemTouchHelper.ItemTouchMoveListener;
import com.jin123d.listener.MyItemClickListener;
import com.jin123d.listener.MyItemLongClickListener;
import com.jin123d.viewHolder.MyViewHolder;

import java.util.Collections;
import java.util.List;

/**
 * Created by jin123d on 2016/2/29.
 */
public class DevicesListAdapter extends RecyclerView.Adapter<MyViewHolder> implements ItemTouchHelperAdapterListener, ItemTouchMoveListener {
    private List<MainBean> lists;
    private MyItemClickListener mItemClickListener;
    private MyItemLongClickListener mItemLongClickListener;
    private LayoutInflater inflater;
    private Context context;

    public DevicesListAdapter(Context context, List<MainBean> lists) {
        this.lists = lists;
        this.context = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = inflater.inflate(R.layout.layout_item_devices, parent, false);
        MyViewHolder vh = new MyViewHolder(itemView, mItemClickListener, mItemLongClickListener);
        return vh;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.tv.setText(lists.get(position).getName());
    }


    @Override
    public int getItemCount() {
        return lists.size();
    }

    public void setOnItemClickListener(MyItemClickListener listener) {
        this.mItemClickListener = listener;
    }

    public void setOnItemLongClickListener(MyItemLongClickListener listener) {
        this.mItemLongClickListener = listener;
    }


    @Override
    public void onItemMove(RecyclerView.ViewHolder source, final int fromPosition, final int toPosition) {
        //切换位置
        // Collections.swap(lists, fromPosition, toPosition);
        //notifyItemMoved(fromPosition, toPosition);


    }

    @Override
    public void onItemDismiss(int position) {

    }

    @Override
    public void onItemMoveOver(RecyclerView.ViewHolder viewHolder, final int fromPosition, final int toPosition) {
        int formType = lists.get(fromPosition).getType();
        int toType = lists.get(toPosition).getType();
        //保证起始为设备
        if (MainBean.TYPE_DEVICES == formType) {
            //目标位置为设备，新建文件夹
            if (MainBean.TYPE_DEVICES == toType) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context)
                        .setTitle(lists.get(fromPosition).getName() + "---合并---" + lists.get(toPosition).getName()).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //目标设备改名，改状态
                                lists.get(toPosition).setName("新目录");
                                lists.get(toPosition).setType(MainBean.TYPE_FOLDER);
                                //更新Adapter
                                notifyItemChanged(toPosition);
                                //移除起始的
                                lists.remove(fromPosition);
                                //更新Adapter
                                notifyItemRemoved(fromPosition);
                            }
                        });
                builder.create().show();
            } else {
                //目标位置为目录，remove，然后加入目录
                AlertDialog.Builder builder = new AlertDialog.Builder(context)
                        .setTitle(lists.get(fromPosition).getName() + "---加入---" + lists.get(toPosition).getName()).setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //移除起始
                                lists.remove(fromPosition);
                                //更新Adapter
                                notifyItemRemoved(fromPosition);
                            }
                        });
                builder.create().show();

            }
        }
    }

}
