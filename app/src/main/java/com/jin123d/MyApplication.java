package com.jin123d;

import android.app.Application;

import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.tweetui.TweetUi;

import io.fabric.sdk.android.Fabric;

/**
 * Created by jin123d on 2016/3/8.
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        TwitterAuthConfig authConfig = new TwitterAuthConfig("cmr7l985tCMMgiPWHteXb36u1", "hlLqD6Wmt5cWli5sBgOU9G6VUJJPZCmb9MdZAys882kuwjKbxb");
        Fabric.with(this, new TwitterCore(authConfig));

        // Example: multiple kits
        Fabric.with(this, new TwitterCore(authConfig), new TweetUi());

    }
}
