

package com.jin123d.itemTouchHelper;

import android.support.v7.widget.RecyclerView;

public interface ItemTouchHelperAdapterListener {

    //拖拽  初始的viewHolder
    void onItemMove(RecyclerView.ViewHolder source,int fromPosition, int toPosition);

    //删除
    void onItemDismiss(int position);
}