# SlidingmenuLib
精简版的slidingmenu类库
模仿QQ代码
<pre>
    slidingMenu
        .setBehindCanvasTransformer(new SlidingMenu.CanvasTransformer() {

					@Override
					public void transformCanvas(Canvas canvas, float percentOpen) {
						float scale = (float) (percentOpen * 0.25 + 0.75);
						canvas.scale(scale, scale, -canvas.getWidth() / 2,
								canvas.getHeight() / 2);
					}
				});

		slidingMenu
				.setAboveCanvasTransformer(new SlidingMenu.CanvasTransformer() {

					@Override
					public void transformCanvas(Canvas canvas, float percentOpen) {
						float scale = (float) (1 - percentOpen * 0.25);
						canvas.scale(scale, scale, 0, canvas.getHeight() / 2);
					}
				});

</pre>



